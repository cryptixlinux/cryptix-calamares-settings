# CryptixLinux calamares settings
## Description
Cryptix configuration files for calmares installer
## Roadmap
[x] - Create project template 

- [x] - Add License
- [ ] - Test partition and mount modules
- [ ] - Test unpackfs module
- [ ] - Test fstab module
- [ ] - Test initcpio modules
- [ ] - Test luks modules
- [ ] - Create custom branding 
- [ ] - Configure packages module
- [ ] - Configure display-manager module
- [ ] - Configure users module
- [ ] - Configure removeuser module
- [ ] - Configure grubcfg module
- [ ] - Configure bootloader module
- [ ] - Configure bootloader-grub module
- [ ] - Configure preservefiles module
- [ ] - Configure umount module
- [ ] - Configure tracking module
- [ ] - Configure finished module
- [ ] - Configure webview module
- [ ] - Configure ucode module
- [ ] - Configure packages-system-update module
- [ ] - Configure packages-no-system-update module
- [ ] - Configure netinstall module
- [ ] - Add runit init system
- [ ] - Add OpenRC init system
## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/CryptixLinux)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## License
[*GNU General Public License v3*](LICENSE)
